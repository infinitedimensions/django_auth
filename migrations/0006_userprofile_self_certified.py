# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_auth', '0005_userprofile_is_investor'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='self_certified',
            field=models.CharField(blank=True, max_length=100, null=True, choices=[(b'high_net_worth', b'Certified High Net Worth Individual'), (b'sophisticated', b'Self-Certified Sophisticated Investor')]),
        ),
    ]
