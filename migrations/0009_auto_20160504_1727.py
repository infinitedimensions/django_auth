# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_auth', '0008_auto_20160504_1719'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='bussiness_experience',
            new_name='business_experience',
        ),
    ]
