# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_auth', '0007_auto_20160422_1856'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='academic_background',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='bussiness_experience',
            field=models.TextField(null=True, blank=True),
        ),
    ]
