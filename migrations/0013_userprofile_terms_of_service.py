# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('django_auth', '0012_auto_20160620_1807'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='terms_of_service',
            field=models.BooleanField(default=False),
        ),
    ]
