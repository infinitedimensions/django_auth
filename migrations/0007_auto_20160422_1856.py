# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_auth', '0006_userprofile_self_certified'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='profile_image',
            field=models.ImageField(default=b'user_avatar.png', upload_to=b'uploads'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='self_certified',
            field=models.CharField(blank=True, max_length=100, null=True, choices=[(b'high_net_worth_individual', b'Certified High Net Worth Individual'), (b'sophisticated_investor', b'Self-Certified Sophisticated Investor')]),
        ),
    ]
