from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):

    SELF_CERTIFIED_CHOICES = (
        ('high_net_worth_individual', 'Certified High Net Worth Individual'),
        ('sophisticated_investor', 'Self-Certified Sophisticated Investor')
    )
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    email = models.CharField(max_length=255, null=True, blank=True)
    user = models.OneToOneField(User, null=True, blank=True)
    activation_key = models.CharField(max_length=40, null=True, blank=True)
    key_expires = models.DateTimeField(null=True, blank=True)
    phone_number = models.CharField(max_length=40, default="000")
    sms_activation = models.CharField(max_length=40, default="000")
    username = models.CharField(max_length=40, default="000")
    is_investor = models.BooleanField(default=False)
    self_certified = models.CharField(choices=SELF_CERTIFIED_CHOICES, max_length=100, blank=True, null=True)
    profile_image = models.ImageField(upload_to='uploads', default='user_avatar.png', null=True, blank=True)
    academic_background = models.TextField(null=True, blank=True)
    business_experience = models.TextField(null=True, blank=True)
    terms_of_service = models.BooleanField(default=False)

    def __str__(self):
        return self.username